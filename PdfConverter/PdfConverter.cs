﻿using System.Drawing;
using System.IO;
using ImageMagick;

public class PdfUtility
{
    public int Width;
    public int Height;

    private MagickImageCollection _images;
    private MagickReadSettings _settings;
    private int _density = 100;

    public PdfUtility(string pdfPath, int density)
    {
        _density = density;
        MagickNET.SetGhostscriptDirectory(Directory.GetCurrentDirectory());

        _settings = new MagickReadSettings
        {
            Density = new Density(_density, _density),
            CompressionMethod = CompressionMethod.DXT1,
        };

        _images = new MagickImageCollection(pdfPath, _settings);
        Width = _images[0].Width;
        Height = _images[0].Height;
    }

    public PdfUtility(string pdfPath)
    {
        MagickNET.SetGhostscriptDirectory(Directory.GetCurrentDirectory());

        _settings = new MagickReadSettings
        {
            Density = new Density(_density, _density),
            CompressionMethod = CompressionMethod.DXT1,
        };

        _images = new MagickImageCollection(pdfPath, _settings);
        Width = _images[0].Width;
        Height = _images[0].Height;
    }

    public void SaveAsImages(string savePath)
    {
        MagickColor fromColor = new MagickColor(Color.Transparent);
        MagickColor toColor = new MagickColor(Color.White);
        var count = 1;

        foreach (var image in _images)
        {
            image.Opaque(fromColor, toColor);
            image.Format = MagickFormat.Dds;
            image.CompressionMethod = CompressionMethod.DXT1;

            var filePath = Path.Combine(savePath, count + ".dds");
            image.Write(filePath);
            count++;
        }
    }

    public PdfPage GetPdfBytes(int page)
    {
        if (_images.Count <= page)
            return null;

        _settings.FrameIndex = page;
        var pdfPage = new PdfPage
        {
            Data = _images[page].ToByteArray(MagickFormat.Dxt1),
            Height = _images[page].Height,
            Width = _images[page].Width
        };

        return pdfPage;
    }

    public static PdfPage GetPdfPage(string file, int pageNumber, int density)
    {
        var settings = new MagickReadSettings
        {
            Density = new Density(density, density),
            FrameIndex = pageNumber,
            FrameCount = 1,
            CompressionMethod = CompressionMethod.DXT5
        };

        using (MagickImage image = new MagickImage(file, settings))
        {
            var pdfPage = new PdfPage
            {
                Data = image.ToByteArray(MagickFormat.Dxt1),
                Height = image.Height,
                Width = image.Width
            };
            return pdfPage;
        }
    }
}

public class PdfPage
{
    public byte[] Data;

    public int Width;

    public int Height;
}