﻿using System;
using System.IO;

namespace PdfConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            if(!Directory.Exists("Pdf"))
            {
                Console.WriteLine("Pdf directory not found");
                Console.ReadKey();

                return;
            }

            var pdfFiles = Directory.GetFiles("Pdf", "*.pdf");

            if (pdfFiles.Length == 0)
            {
                Console.WriteLine("Pdf file not found");
                Console.ReadKey();
                return;
            }

            string _pdfPath = pdfFiles[0];
            string _savePath = @"Pdf/Pages";

            if (!Directory.Exists(_savePath))
            {
                Directory.CreateDirectory(_savePath);
            }

            var density = 200;

            if(args.Length > 0)
            {
                int.TryParse(args[0], out density);
            }

            PdfUtility _pdfUtility = new PdfUtility(_pdfPath, density);
            _pdfUtility.SaveAsImages(_savePath);
        }
    }
}
